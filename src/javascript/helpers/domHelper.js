export function createElement(domElement) {
    const element = document.createElement(domElement.tagName);
    if (domElement.className !== undefined) {
        element.classList.add(domElement.className);
    }
    if (domElement.attributes !== undefined) {
        domElement.attributes.forEach((value, key) => element.setAttribute(key, value));
    }
    return element;
}
