export function fight(firstFighter, secondFighter) {
    const backupHealthFirstFighter = firstFighter.health;
    const backupHealthSecondFighter = secondFighter.health;
    let winner;
    while (true) {
        let isKillingPunch = punch(firstFighter, secondFighter);
        if (isKillingPunch) {
            winner = firstFighter;
            break;
        }
        isKillingPunch = punch(secondFighter, firstFighter);
        if (isKillingPunch) {
            winner = secondFighter;
            break;
        }
    }
    firstFighter.health = backupHealthFirstFighter;
    secondFighter.health = backupHealthSecondFighter;
    return winner;
}
export function getDamage(attacker, enemy) {
    return getHitPower(attacker) - getBlockPower(enemy);
}
export function getHitPower(fighter) {
    return fighter.attack * getChance();
}
export function getBlockPower(fighter) {
    return fighter.defense * getChance();
}
function getChance() {
    return Math.random() + 1; // [1, 2)
}
function punch(attacker, enemy) {
    let damage = getDamage(attacker, enemy);
    if (damage > 0) {
        enemy.health -= damage;
    }
    if (enemy.health <= 0) {
        return true;
    }
    return false;
}
