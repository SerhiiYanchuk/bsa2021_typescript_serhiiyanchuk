import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService';
export function createFighters(fighters) {
    const selectFighterForBattle = createFightersSelector();
    const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
    const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });
    fightersContainer.append(...fighterElements);
    return fightersContainer;
}
const fightersDetailsCache = new Map();
async function showFighterDetails(event, fighter) {
    const fullInfo = await getFighterInfo(fighter._id);
    showFighterDetailsModal(fullInfo);
}
export async function getFighterInfo(fighterId) {
    // get fighter form fightersDetailsCache or use getFighterDetails function
    if (fightersDetailsCache.has(fighterId))
        return fightersDetailsCache.get(fighterId);
    let fighter = await getFighterDetails(fighterId);
    fightersDetailsCache.set(fighterId, fighter);
    return fighter;
}
function createFightersSelector() {
    const selectedFighters = new Map();
    return async function selectFighterForBattle(event, fighter) {
        const fullInfo = await getFighterInfo(fighter._id);
        if (event.target !== null) {
            let checkbox = event.target;
            if (checkbox.checked) {
                selectedFighters.set(fighter._id, [fullInfo, checkbox]);
            }
            else {
                selectedFighters.delete(fighter._id);
            }
            if (selectedFighters.size === 2) {
                const fighters = Array.from(selectedFighters.values());
                const winner = fight(fighters[0][0], fighters[1][0]);
                showWinnerModal(winner);
                fighters[0][1].checked = false;
                fighters[1][1].checked = false;
                selectedFighters.clear();
            }
        }
    };
}
