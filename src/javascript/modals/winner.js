import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showWinnerModal(fighter) {
    // show winner name and image
    const title = 'The winner of cookies';
    const bodyElement = createWinnerFighterDetails(fighter);
    showModal({ title, bodyElement });
}
function createWinnerFighterDetails(fighter) {
    const { name, source } = fighter;
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const attributes = new Map([["src", source]]);
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image-winner', attributes });
    nameElement.innerText = `Name: ${name}`;
    fighterDetails.append(nameElement);
    fighterDetails.append(imageElement);
    return fighterDetails;
}
