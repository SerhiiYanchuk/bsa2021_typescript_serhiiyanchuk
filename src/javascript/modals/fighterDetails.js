import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
export function showFighterDetailsModal(fighter) {
    const title = 'Fighter info';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}
function createFighterDetails(fighter) {
    const { name, attack, defense, health, source } = fighter;
    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
    const attacklement = createElement({ tagName: 'span', className: 'fighter-attack' });
    const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
    const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
    const attributes = new Map([["src", source]]);
    const imageElement = createElement({ tagName: 'img', className: 'fighter-image-detail', attributes });
    nameElement.innerText = `Name: ${name}`;
    attacklement.innerText = `Attack: ${attack.toString()}`;
    ;
    defenseElement.innerText = `Defense: ${defense.toString()}`;
    ;
    healthElement.innerText = `Health: ${health.toString()}`;
    ;
    fighterDetails.append(nameElement);
    fighterDetails.append(attacklement);
    fighterDetails.append(defenseElement);
    fighterDetails.append(healthElement);
    fighterDetails.append(imageElement);
    return fighterDetails;
}
