import { IFighter } from "./models/fighterModel";

export function fight(firstFighter: IFighter, secondFighter: IFighter): IFighter {
  const backupHealthFirstFighter = firstFighter.health; 
  const backupHealthSecondFighter = secondFighter.health; 
  let winner: IFighter;

  while (true) {
    let isKillingPunch: boolean = punch(firstFighter, secondFighter);
    if (isKillingPunch){
      winner = firstFighter;
      break;
    }
    isKillingPunch = punch(secondFighter, firstFighter);
    if (isKillingPunch){
      winner = secondFighter;
      break;
    }
  }
  
  firstFighter.health = backupHealthFirstFighter;
  secondFighter.health = backupHealthSecondFighter;
  return winner;
}

export function getDamage(attacker: IFighter, enemy: IFighter): number {
  return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter: IFighter): number {
  return fighter.attack * getChance();
}

export function getBlockPower(fighter: IFighter): number {
  return fighter.defense * getChance();
}

function getChance(): number {
  return Math.random() + 1;   // [1, 2)
}

function punch(attacker: IFighter, enemy: IFighter): boolean {
  let damage = getDamage(attacker, enemy);
  if (damage > 0) {
    enemy.health -= damage;
  }
  if (enemy.health <= 0) {
    return true;
  }
  return false;
}