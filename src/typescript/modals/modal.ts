import { createElement } from '../helpers/domHelper';
import { IModalInfo } from '../models/modalModel';

export function showModal(modalInfo: IModalInfo) {

  const root = getModalContainer();
  const modal = createModal(modalInfo); 
  
  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById('root') as HTMLElement;
}

function createModal(modalInfo: IModalInfo): HTMLElement {
  let { title, bodyElement } = modalInfo
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string): HTMLElement {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal(event: Event): void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
