import { getFighters } from './services/fightersService'
import { createFighters } from './fightersView';
import { IFighterMainInfo } from './models/fighterModel';

const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');

export async function startApp() {
  if (rootElement !== null && loadingElement !== null)
  {
    try {
      loadingElement.style.visibility = 'visible';
  
      const fighters: IFighterMainInfo[] = await getFighters();
      const fightersElement = createFighters(fighters);
      
      rootElement.appendChild(fightersElement);

    } catch (error) {
      console.warn(error);
      rootElement.innerText = 'Failed to load data';
    } finally {
      loadingElement.style.visibility = 'hidden';
    }
  } 
}
