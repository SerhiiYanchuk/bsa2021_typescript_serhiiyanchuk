import { IDomElement } from "../models/domElementModel";

export function createElement(domElement: IDomElement): HTMLElement {
  const element = document.createElement(domElement.tagName);
  
  if (domElement.className !== undefined) {
    element.classList.add(domElement.className);
  }
  if (domElement.attributes !== undefined) {
    domElement.attributes.forEach((value: string, key: string) => element.setAttribute(key, value));
  } 
  return element;
}