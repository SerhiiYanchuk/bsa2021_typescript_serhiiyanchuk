import { fightersDetails, fighters } from './mockData';
import { IFighterMainInfo, IFighter } from '../models/fighterModel';

const API_URL: string = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI: boolean = true;

type HttpMethod = "GET" | "POST" | "PUT" | "DELETE";
type ReturnApiValue = IFighterMainInfo[] | IFighter;

async function callApi(endpoint: string, method: HttpMethod): Promise<ReturnApiValue> {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response: Response) => (response.ok ? response.json() as Promise<{ content: string }> : Promise.reject(Error('Failed to load'))))
        .then((result: { content: string }) => JSON.parse(atob(result.content)) as ReturnApiValue)
        .catch((error: Error) => {
          throw error;
        });
}

async function fakeCallApi(endpoint: string): Promise<ReturnApiValue> {
  const response: ReturnApiValue = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): IFighter {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id: number = Number(endpoint.substring(start + 1, end));

  return fightersDetails.find((it) => it._id === id) as IFighter;
}

export { callApi };
