import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighter, IFighterMainInfo } from './models/fighterModel';
import { getFighterDetails } from './services/fightersService';
import { IBasicFighterEvent } from './models/eventModels';

export function createFighters(fighters: IFighterMainInfo[]): HTMLElement {
  const selectFighterForBattle: IBasicFighterEvent = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache: Map<number, IFighter> = new Map<number, IFighter>();

async function showFighterDetails(event: Event, fighter: IFighterMainInfo): Promise<void> {
  const fullInfo: IFighter = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: number): Promise<IFighter>  {
  // get fighter form fightersDetailsCache or use getFighterDetails function
  if (fightersDetailsCache.has(fighterId))
    return fightersDetailsCache.get(fighterId) as IFighter;
  
  let fighter = await getFighterDetails(fighterId);
  fightersDetailsCache.set(fighterId, fighter);
  return fighter;
}

function createFightersSelector() {
  type FighterWithCheckbox = [IFighter, HTMLInputElement];
  const selectedFighters: Map<number, FighterWithCheckbox> = new Map<number, FighterWithCheckbox>();

  return async function selectFighterForBattle(event: Event, fighter: IFighterMainInfo): Promise<void> {
    const fullInfo = await getFighterInfo(fighter._id);
    if (event.target !== null)
    {
      let checkbox = <HTMLInputElement>event.target;
      if (checkbox.checked) {
        selectedFighters.set(fighter._id, [fullInfo, checkbox]);
      } else { 
        selectedFighters.delete(fighter._id);
      }
      if (selectedFighters.size === 2) {
        const fighters = Array.from(selectedFighters.values());
        const winner = fight(fighters[0][0], fighters[1][0]);

        showWinnerModal(winner);
        fighters[0][1].checked = false;
        fighters[1][1].checked = false;
        selectedFighters.clear();
      }
    }   
  }
}
