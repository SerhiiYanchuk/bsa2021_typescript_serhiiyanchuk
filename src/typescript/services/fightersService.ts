import { callApi } from '../helpers/apiHelper';
import { IFighterMainInfo, IFighter } from '../models/fighterModel';

export async function getFighters(): Promise<IFighterMainInfo[]> {
  try {
    const endpoint: string = 'fighters.json';
    const apiResult: IFighterMainInfo[] = await callApi(endpoint, 'GET') as IFighterMainInfo[];
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: number): Promise<IFighter> {
  // endpoint - `details/fighter/${id}.json`;
  try {
    const endpoint: string = `details/fighter/${id}.json`;
    const apiResult: IFighter = await callApi(endpoint, 'GET') as IFighter;
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}