
export interface IDomElement {
    tagName: string;
    className?: string;
    attributes?: Map<string, string>;
}