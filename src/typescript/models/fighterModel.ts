export interface IFighterMainInfo {
    _id: number;
    name: string;
    source: string;
}

export interface IFighter extends IFighterMainInfo {
    health: number;
    attack: number;
    defense: number;
}