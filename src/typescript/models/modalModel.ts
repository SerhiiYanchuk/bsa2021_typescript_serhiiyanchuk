
export interface IModalInfo {
    title: string;
    bodyElement: HTMLElement;
}