import { IFighterMainInfo } from "./fighterModel";

export interface IBasicFighterEvent {
    (event: Event, fighter: IFighterMainInfo): Promise<void>;
}